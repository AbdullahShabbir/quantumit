﻿using QuantumIT.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace QuantumIT.Controllers
{
    [RoutePrefix("restfull/api")]
    public class RestfullController : ApiController
    {
        [Route("classes")]
        [HttpGet]
        public HttpResponseMessage ListClass()
        {
            try
            {
                DatabaseContext databaseContect = new DatabaseContext();

                var classes = databaseContect.Classes;
                return Request.CreateResponse(HttpStatusCode.OK, classes);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

        }

        [Route("students")]
        [HttpGet]
        public HttpResponseMessage ListStudents()
        {
            try
            {
                DatabaseContext databaseContect = new DatabaseContext();

                var classes = databaseContect.Students;
                return Request.CreateResponse(HttpStatusCode.OK, classes);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

        }
    }
}
