﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Data.Entity;
using Newtonsoft.Json;
using QuantumIT.Models;

namespace QuantumIT.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult LoadClasses()
        {
            DatabaseContext databaseContext = new DatabaseContext();
            var classes = databaseContext.Classes.ToList();

            return Json(classes, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddClass(Class model)
        {
            DatabaseContext databaseContext = new DatabaseContext();
            model.Id = databaseContext.Classes.ToList().Count + 1;
            databaseContext.Classes.Add(model);
            databaseContext.SaveChanges();
            
            return Json(new { success = true, msg = "Class Added Successfully." });
        }

        [HttpPost]
        public JsonResult EditClass(Class model)
        {
            DatabaseContext databaseContext = new DatabaseContext();
            databaseContext.Classes.Attach(model);

            var entry = databaseContext.Entry(model);
            entry.State = EntityState.Modified;
            
            databaseContext.SaveChanges();

            return Json(new { success = true, msg = "Class Updated Successfully." });
        }

        [HttpPost]
        public JsonResult DeleteClass(Class model)
        {
            DatabaseContext databaseContext = new DatabaseContext();
            var entity = databaseContext.Classes.Where(c => c.Id == model.Id).FirstOrDefault();
            databaseContext.Classes.Remove(entity);

            databaseContext.SaveChanges();
            
            return Json(new { success = true, msg = "Class Deleted Successfully." });
        }

        [HttpPost]
        public JsonResult LoadEnrolledStudents(int id)
        {
            DatabaseContext databaseContext = new DatabaseContext();
            
            var studentList = (from c in databaseContext.Classes
                        from s in c.Students
                        where c.Id == id
                        select s).ToList();
            
            return Json(studentList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddStudent(Student model, int classId)
        {
            DatabaseContext databaseContext = new DatabaseContext();
            model.Id = databaseContext.Students.ToList().Count + 1;

            var entity = databaseContext.Classes.Where(c => c.Id == classId).FirstOrDefault();
            entity.Students.Add(model);

            databaseContext.Students.Add(model);

            databaseContext.SaveChanges();

            return Json(new { success = true, msg = "Class Added Successfully." });
        }

        [HttpPost]
        public JsonResult IsSurnameUnique(Student model, int classId)
        {
            DatabaseContext databaseContext = new DatabaseContext();

            var studentList = (from c in databaseContext.Classes
                               from s in c.Students
                               where c.Id == classId && s.Id != model.Id
                               select s).ToList();

            Student student = studentList.Where(s => s.Surname.ToLower() == model.Surname.ToLower()).FirstOrDefault();

            if (student == null)
                return Json(true);
            else
                return Json(false);
        }

        [HttpPost]
        public JsonResult EditStudent(Student model)
        {
            DatabaseContext databaseContext = new DatabaseContext();
            databaseContext.Students.Attach(model);

            var entry = databaseContext.Entry(model);
            entry.State = EntityState.Modified;

            databaseContext.SaveChanges();

            return Json(new { success = true, msg = "Student Updated Successfully." });
        }

        [HttpPost]
        public JsonResult DeleteStudent(Student model, int classId)
        {
            DatabaseContext databaseContext = new DatabaseContext();
            
            var studentList = (from c in databaseContext.Classes
                               from s in c.Students
                               where c.Id == classId
                               select s).ToList();

            var student = studentList.Where(s => s.Id == model.Id).FirstOrDefault();
            databaseContext.Students.Remove(student);

            databaseContext.SaveChanges();

            return Json(new { success = true, msg = "Student Deleted Successfully." });
        }

        public ActionResult RestfullAPI()
        {
            return View();
        }

        public async Task<ActionResult> CallAPI()
        {
            string json = "";
            using (HttpClient client = new HttpClient())
            {
                string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("restfull/api/classes");

                if (response.IsSuccessStatusCode)
                    json = await response.Content.ReadAsStringAsync();
                else
                    response.EnsureSuccessStatusCode();
            }

            ViewBag.JSONResult = Json(JsonConvert.SerializeObject(json), JsonRequestBehavior.AllowGet).Data;

            return View("RestfullAPI");
        }
    }
}