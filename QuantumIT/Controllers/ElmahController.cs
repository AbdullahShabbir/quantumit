﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QuantumIT.Models;

namespace QuantumIT.Controllers
{
    public class ElmahController : Controller
    {
        private DatabaseContext db = new DatabaseContext();

        // GET: Elmah
        public ActionResult Index()
        {
            return View(db.ElmahErrors.ToList());
        }

        // GET: Elmah/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ElmahError elmahError = db.ElmahErrors.Find(id);
            if (elmahError == null)
            {
                return HttpNotFound();
            }
            return View(elmahError);
        }

        // GET: Elmah/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Elmah/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ErrorId,Application,Host,Type,Source,Message,User,StatusCode,TimeUtc,Sequence,AllXml")] ElmahError elmahError)
        {
            if (ModelState.IsValid)
            {
                elmahError.ErrorId = Guid.NewGuid();
                db.ElmahErrors.Add(elmahError);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(elmahError);
        }

        // GET: Elmah/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ElmahError elmahError = db.ElmahErrors.Find(id);
            if (elmahError == null)
            {
                return HttpNotFound();
            }
            return View(elmahError);
        }

        // POST: Elmah/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ErrorId,Application,Host,Type,Source,Message,User,StatusCode,TimeUtc,Sequence,AllXml")] ElmahError elmahError)
        {
            if (ModelState.IsValid)
            {
                db.Entry(elmahError).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(elmahError);
        }

        // GET: Elmah/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ElmahError elmahError = db.ElmahErrors.Find(id);
            if (elmahError == null)
            {
                return HttpNotFound();
            }
            return View(elmahError);
        }

        // POST: Elmah/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            ElmahError elmahError = db.ElmahErrors.Find(id);
            db.ElmahErrors.Remove(elmahError);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
