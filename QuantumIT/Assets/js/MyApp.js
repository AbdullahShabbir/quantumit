﻿var app = angular.module('myApp', ['ngValidate']);
app.controller('myAppController', function ($scope, $http) {

    /**********************  CLASS SECTION START  **********************/
    
    $scope.addClassFormData = {};       //Used to store Add Class Form Data
    $scope.deleteClassFormData = {};    //Used to store Delete Class Form Data
    $scope.editClassFormData = {};      //Used to store Edit Class Form Data

    //Load all Classes
    $scope.loadClasses = function () {
        $http({
            method: "POST",
            url: "/Home/LoadClasses",
        }).then(function Success(response) {
            $scope.classes = angular.fromJson(response.data);
        });
    };
    
    //Displays Enrolled Students in a Class
    $scope.rowSelected = function (classId, className) {
        $scope.selectedRow = classId;
        $scope.selectedClassName = className;
        $scope.selectedClassId = classId;
        $scope.addStudentFormData.ClassId = classId;
        $scope.editStudentFormData.ClassId = classId;
        $scope.deleteStudentFormData.ClassId = classId;
        $http({
            method: "POST",
            url: "/Home/LoadEnrolledStudents",
            data: { "id": classId }
        }).then(function Success(response) {
            $scope.studentList = angular.fromJson(response.data);
        });
    };

    //Rules for Valid Class
    $scope.classValidationOptions = {
        rules: {
            ClassName: { required: true },
            Location: { required: true },
            TeacherName: { required: true }
        },
        messages: {
            ClassName: { required: "Please enter Class Name" },
            Location: { required: "Please enter the Location" },
            TeacherName: { required: "Please enter Teacher Name" }
        }
    };
    
    //Adds a Class
    $scope.addClassForm = function (form) {
        if (form.validate()) {
            $http({
                method: "POST",
                url: "/Home/AddClass",
                data: $.param($scope.addClassFormData),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function Success(response) {
                var data = angular.fromJson(response.data);
                if (data.success) {
                    $scope.loadClasses();
                    $('#addClass').modal('hide');
                    $scope.resetClassForm("add");
                }
            });
        }
    };

    //Displays Class Data in Edit Class Modal
    $scope.editClass = function (classObj) {
        $('#editClass').modal('show');
        $scope.editClassFormData.Id = classObj.Id;
        $scope.editClassFormData.ClassName = classObj.ClassName;
        $scope.editClassFormData.Location = classObj.Location;
        $scope.editClassFormData.TeacherName = classObj.TeacherName;
    }
    
    //Edits a Class
    $scope.editClassForm = function (form) {
        if (form.validate()) {
            $http({
                method: "POST",
                url: "/Home/EditClass",
                data: $.param($scope.editClassFormData),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function Success(response) {
                var data = angular.fromJson(response.data);
                if (data.success) {
                    $scope.loadClasses();
                    $('#editClass').modal('hide');
                    $scope.resetClassForm("edit");
                }
            });
        }
    };

    //Displays Class Data in Delete Class Modal
    $scope.deleteClass = function (classObj) {
        $('#deleteClass').modal('show');
        $scope.deleteClassFormData.Id = classObj.Id;
        $scope.deleteClassFormData.ClassName = classObj.ClassName;
        $scope.deleteClassFormData.Location = classObj.Location;
        $scope.deleteClassFormData.TeacherName = classObj.TeacherName;
    }
    
    //Deletes a Class
    $scope.deleteClassForm = function (form) {
        //if (form.validate()) {
        $http({
            method: "POST",
            url: "/Home/DeleteClass",
            data: $.param($scope.deleteClassFormData),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function Success(response) {
            var data = angular.fromJson(response.data);
            if (data.success) {
                $scope.loadClasses();
                $('#deleteClass').modal('hide');
                $scope.resetClassForm("delete");
                //toastr.success(data.msg, 'Success');
            } else {
                //toastr.error(data.msg, 'Error');
            }
        });
        //}
    };

    //Clears Class Form
    $scope.resetClassForm = function (method) {
        if (method == "add") {
            $scope.addClassFormData.ClassName = "";
            $scope.addClassFormData.Location = "";
            $scope.addClassFormData.TeacherName = "";
            $("#addclassform").validate().resetForm();
        }
        else if (method == "edit") {
            $scope.editClassFormData.Id = "";
            $scope.editClassFormData.ClassName = "";
            $scope.editClassFormData.Location = "";
            $scope.editClassFormData.TeacherName = "";
            $("#editclassform").validate().resetForm();
        }
        else {
            $scope.deleteClassFormData.Id = "";
            $scope.deleteClassFormData.ClassName = "";
            $scope.deleteClassFormData.Location = "";
            $scope.deleteClassFormData.TeacherName = "";
            $("#deleteclassform").validate().resetForm();
        }
    }

    /**********************  CLASS SECTION END  **********************/

    /**********************  STUDENT SECTION START  **********************/

    $scope.addStudentFormData = {};         //Used to store Add Student Form Data
    $scope.deleteStudentFormData = {};      //Used to store Delete Student Form Data
    $scope.editStudentFormData = {};        //Used to store Edit Student Form Data

    //Load all Students from ClassID
    $scope.loadStudent = function (classId) {
        $http({
            method: "POST",
            url: "/Home/LoadEnrolledStudents",
            data: { "id": classId }
        }).then(function Success(response) {
            $scope.studentList = angular.fromJson(response.data);
        });
    };

    //Rules for a Valid Student
    $scope.studentValidationOptions = {
        rules: {
            FirstName: { required: true },
            Surname: { required: true },
            Age: { required: true, range: [0, 100], digits: true },
            GPA: { required: true, number: true }
        },
        messages: {
            FirstName: { required: "Please enter your First Name" },
            Surname: { required: "Please enter your Surname" },
            Age: {
                required: "Please enter your Age",
                range: "Age can only be between 0 and 100",
                digits: "Only Numbers are allowed"
            },
            GPA: {
                required: "Please enter your GPA",
                number: "Only Numbers are allowed"
            }
        }
    };

    //Checks Unique Surname of Student while adding a new Student
    $scope.checkAddIsSurnameUnique = function (student) {
        $http({
            method: "POST",
            url: "/Home/IsSurnameUnique",
            data: $.param(student),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function Success(response) {
            if (angular.fromJson(response.data)) {
                $scope.addIsSurnameUnique = false;
            } else {
                $scope.addIsSurnameUnique = true;
            }
        });
    }

    //Checks Unique Surname of Student while editing a Student
    $scope.checkEditIsSurnameUnique = function (student) {
        //console.log(student);
        $http({
            method: "POST",
            url: "/Home/IsSurnameUnique",
            data: $.param(student),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function Success(response) {
            if (angular.fromJson(response.data)) {
                $scope.editIsSurnameUnique = false;
            } else {
                $scope.editIsSurnameUnique = true;
            }
        });
    }

    //Adds a New Student
    $scope.addStudentForm = function (form) {
        if (form.validate() && !$scope.addIsSurnameUnique) {
            $http({
                method: "POST",
                url: "/Home/AddStudent",
                data: $.param($scope.addStudentFormData),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function mySuccess(response) {
                var data = angular.fromJson(response.data);
                if (data.success) {
                    $('#addStudent').modal('hide');
                    $scope.resetStudentForm("add");
                    $scope.loadStudent($scope.addStudentFormData.ClassId);
                } 
            });
        }
    };

    //Displays Student Data in Edit Modal
    $scope.editStudent = function (studentObj) {
        $('#editStudent').modal('show');
        $scope.editStudentFormData.Id = studentObj.Id;
        $scope.editStudentFormData.FirstName = studentObj.FirstName;
        $scope.editStudentFormData.Surname = studentObj.Surname;
        $scope.editStudentFormData.Age = studentObj.Age;
        $scope.editStudentFormData.GPA = studentObj.GPA;
    }

    //Edits a Student
    $scope.editStudentForm = function (form) {
        if (form.validate() && !$scope.editIsSurnameUnique) {
            $http({
                method: "POST",
                url: "/Home/EditStudent",
                data: $.param($scope.editStudentFormData),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function Success(response) {
                var data = angular.fromJson(response.data);
                if (data.success) {
                    $scope.loadStudent($scope.editStudentFormData.ClassId);
                    $('#editStudent').modal('hide');
                    $scope.resetStudentForm("edit");
                }
            });
        }
    };

    //Displays Student Data in Delete Modal
    $scope.deleteStudent = function (studentObj) {
        $('#deleteStudent').modal('show');
        $scope.deleteStudentFormData.Id = studentObj.Id;
        $scope.deleteStudentFormData.FirstName = studentObj.FirstName;
        $scope.deleteStudentFormData.Surname = studentObj.Surname;
        $scope.deleteStudentFormData.Age = studentObj.Age;
        $scope.deleteStudentFormData.GPA = studentObj.GPA;
    }
    
    //Deletes a Student from a Class
    $scope.deleteStudentForm = function (form) {
        $http({
            method: "POST",
            url: "/Home/DeleteStudent",
            data: $.param($scope.deleteStudentFormData),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function Success(response) {
            var data = angular.fromJson(response.data);
            if (data.success) {
                $scope.loadStudent($scope.deleteStudentFormData.ClassId);
                $('#deleteStudent').modal('hide');
                $scope.resetStudentForm("delete");
            }
        });
    };
    
    //Clears Student Form
    $scope.resetStudentForm = function (method) {
        if (method == "add") {
            $scope.addStudentFormData.FirstName = "";
            $scope.addStudentFormData.Surname = "";
            $scope.addStudentFormData.Age = "";
            $scope.addStudentFormData.GPA = "";
            $("#addstudentform").validate().resetForm();
        }
        else if (method == "edit") {
            $scope.editStudentFormData.Id = "";
            $scope.editStudentFormData.FirstName = "";
            $scope.editStudentFormData.Surname = "";
            $scope.editStudentFormData.Age = "";
            $scope.editStudentFormData.GPA = "";
            $("#editstudentform").validate().resetForm();
        }
        else {
            $scope.deleteStudentFormData.Id = "";
            $scope.deleteStudentFormData.FirstName = "";
            $scope.deleteStudentFormData.Surname = "";
            $scope.deleteStudentFormData.Age = "";
            $scope.deleteStudentFormData.GPA = "";
            $("#deletestudentform").validate().resetForm();
        }
    }

    $('#editStudent').on('hidden.bs.modal', function () {
        $scope.editIsSurnameUnique = false;
    });
});  

/**********************  STUDENT SECTION END  **********************/