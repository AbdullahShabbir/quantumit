﻿using System.Collections.Generic;

namespace QuantumIT.Models
{
    public class Class
    {
        public Class()
        {
            Students = new List<Student>();
        }

        public int Id { get; set; }

        public string ClassName { get; set; }
        public string Location { get; set; }
        public string TeacherName { get; set; }

        public List<Student> Students { get; set; }
    }
}