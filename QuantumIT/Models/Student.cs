﻿using System.Collections.Generic;

namespace QuantumIT.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public double GPA { get; set; }
        
        public List<Class> Classes { get; set; }
    }
}