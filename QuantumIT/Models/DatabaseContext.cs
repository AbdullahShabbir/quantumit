﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.ModelConfiguration;

namespace QuantumIT.Models
{
    public class DatabaseContext : DbContext
    {
        private readonly string _tableName;
        private readonly string _schemaName;

        public DatabaseContext() : base("name=DatabaseContext")
        {
            Database.SetInitializer(new DatabaseInitializer());
        }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<ElmahError>().MapToStoredProcedures(s => s.Insert(u => u.HasName("ELMAH_LogError", "dbo")));
        }
        
        public DbSet<Class> Classes { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<ElmahError> ElmahErrors { get; set; }
    }
    
    public class DatabaseInitializer : CreateDatabaseIfNotExists<DatabaseContext>
    {
        protected override void Seed(DatabaseContext context)
        {
            List<Student> bioStudent = new List<Student>()
            {
                new Student() { Id = 1, FirstName = "David", Surname = "Jackson", Age = 19, GPA = 3.4 },
                new Student() { Id = 2, FirstName = "Peter", Surname = "Parker", Age = 19, GPA = 2.9 },
                new Student() { Id = 3, FirstName = "Robert", Surname = "Smith", Age = 18, GPA = 3.1 },
                new Student() { Id = 4, FirstName = "Rebecca", Surname = "Black", Age = 19, GPA = 2.1 }
            };

            List<Student> engishStudent = new List<Student>()
            {
                new Student() { Id = 5, FirstName = "Bruce", Surname = "Lee", Age = 19, GPA = 3.4 },
                new Student() { Id = 6, FirstName = "Paul", Surname = "Jones", Age = 19, GPA = 2.9 },
                new Student() { Id = 7, FirstName = "Aki", Surname = "Thun", Age = 18, GPA = 3.1 },
                new Student() { Id = 8, FirstName = "Max", Surname = "Zhou", Age = 19, GPA = 4.2 }
            };
            
            context.Classes.AddRange(new List<Class>()
            {
                new Class() { Id = 1, ClassName = "Biology", Location = "Building 5 Room 201", TeacherName = "Mr Robertson", Students = bioStudent },
                new Class() { Id = 2, ClassName = "English", Location = "Building 3 Room 134", TeacherName = "Miss Sanderson", Students = engishStudent }
            });
            
            base.Seed(context);
        }
    }
}